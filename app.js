const order = {
    toppings: [],
    basePrice: 80,
    total: 80
}

const toppings = [{
        name: 'pepperoni',
        price: 20
    },{
        name: 'mushroom',
        price: 40
    }, {
        name: 'cheese',
        price: 0
    }, {
        name: 'pineapple',
        price: 30
    }
]

// Cache topping containers
let $pizza  = null;
let $mushrooms = null;
let $pepperoni = null;
let $pineapple = null;

$(document).ready(function () {
    // Write the best jQuery ever here

    $pepperoni= $('.pepperonis');
    $mushrooms =$('.mushrooms');
    $pineapple =$('.pineapples');
    $pizza  =$('.pizza');
  
    
 
        $('#pineapple').click(function(e){
            
            if(order.toppings.includes(toppings[3]))
            {  
              var pos= order.toppings.indexOf(toppings[3]);
               order.toppings.splice(pos,1);
               order.total-=toppings[3].price;
               $('#total').text(order.total);
               $pineapple.html('');
               
            
            }
             else{

                $('.pineapples').html(generateTopping(toppings[3]));
                order.total+=toppings[3].price;
                $('#total').text(order.total);
                 order.toppings.push(toppings[3]);
            
             }
    
            });
    
            $('#pepperoni').click(function()
            {
                if(order.toppings.includes(toppings[0]))
                {  
                     var pos= order.toppings.indexOf(toppings[0]);
                     order.toppings.splice(pos,1);
                     order.total-=toppings[0].price;
                      $('#total').text(order.total);
                      $pepperoni.html('');
                }
            else{

                $('.pepperonis').html(generateTopping(toppings[0]));
                order.total+=toppings[0].price;
                $('#total').text(order.total);
                order.toppings.push(toppings[0]);
                
               }
            });
 
            $('#mushroom').click(function(){

                if(order.toppings.includes(toppings[1]))
                {  
                     var pos= order.toppings.indexOf(toppings[1]);
                     order.toppings.splice(pos,1);
                     order.total-=toppings[1].price;
                      $('#total').text(order.total);
                      $mushrooms.html('');
                }
                 else {   
        
                $('.mushrooms').html(generateTopping(toppings[1]));
                order.total+=toppings[1].price;
                $('#total').text(order.total);
                order.toppings.push(toppings[1]);
                }
                
            });



            $('#cheese').click(function()
            {
              
                $pizza.toggleClass('no-cheese');

            });





});




// Automatically generate the toppings based on the name and id from the button.
function generateTopping(topping) {
    const looper = Array(10).fill(topping.name);
    return looper.map(item => `<div class="${item}"></div>`).join('');
}
